class Van {

    constructor(vanId, capacity, licence, costPerDay, insurance) {
        this.vanId = vanId;
        this.capacity = capacity;
        this.costPerDay = parseInt(costPerDay);
        this.insurance = parseInt(insurance);
        this.licence = licence;
    }

    set vanId(value) {
        this._vanId = value;
    }

    get vanId() {
        return this._vanId;
    }

    set capacity(value) {
        this._capacity = value;
    }

    get capacity() {
        return this._capacity;
    }

    set licence(value) {
       this._licence = value;
    }

    get licence() {
        return this._licence;
    }

    set costPerDay(value) {
        this._costPerDay = parseInt(value);
    }

    get costPerDay() {
        return this._costPerDay;
    }

    set insurance(value) {
        this._insurance = parseInt(value);
    }

    get insurance() {
        return this._insurance;
    }

    display() {
        document.getElementById("resultID").innerHTML = "VAN ID: " + this._vanId;
        document.getElementById("resultCapacity").innerHTML = "CAPACITY M3: " + this._capacity; 
        document.getElementById("resultLicence").innerHTML = "LICENCE: " + this._licence;
        document.getElementById("resultCost").innerHTML = "COST PER DAY: " + this._costPerDay + "$";
        document.getElementById("resultInsurance").innerHTML = "INSURANCE COST: " + this._insurance + "$";
        document.getElementById("resultTotal").innerHTML = "TOTAL COST: " + (parseInt(this._costPerDay) + parseInt(this._insurance)) + "$";
    }

}

var van;

function newVan() {
    var id = document.getElementById("id").value;
    var capacity = document.getElementById("capacity").value;
    var licence = document.getElementById("licence").value;
    var costPerDay = document.getElementById("costPerDay").value;
    var insurance = document.getElementById("insurance").value;
    van = new Van(id,capacity,licence,costPerDay,insurance);
    van.display();
}